package com.movlancer.freelancer.network;

import com.movlancer.freelancer.model.MovieItem;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("movie/popular")
    Call<MovieItem> getMoviesByPopularity(@Query("page") String page, @Query("sort_by") String sort_by, @Query("api_key") String apiKey);

    @GET("search/movie")
    Call<MovieItem> getMoviesByName(@Query("query") String query, @Query("page") String page, @Query("sort_by") String sort, @Query("api_key") String apiKey);

}
