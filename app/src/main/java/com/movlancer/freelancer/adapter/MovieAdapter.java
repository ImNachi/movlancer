package com.movlancer.freelancer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.movlancer.freelancer.R;
import com.movlancer.freelancer.model.Movie;
import com.movlancer.freelancer.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.Holder> {

    private Context context;
    private List<Movie> movieList = new ArrayList<>();

    public MovieAdapter(Context context) {
        this.context = context;
    }

    public void updateMovieAdapter(List<Movie> movies) {
        this.movieList = movies;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new Holder(LayoutInflater.from(context).inflate(R.layout.item_movie, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        holder.bind(this.movieList.get(i));
    }

    @Override
    public int getItemCount() {
        return this.movieList.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageViewMoviePoster)
        ImageView imageViewMoviePoster;
        @BindView(R.id.ratingBarMovieVote)
        RatingBar ratingBarMovieVote;
        @BindView(R.id.textViewMovieTitle)
        TextView textViewMovieTitle;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Movie movie) {
            Glide.with(context)
                    .load(StringUtils.posterPath(movie.getPosterPath()))
                    .apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCorners(20)))
                    .into(imageViewMoviePoster);

            ratingBarMovieVote.setRating((movie.getVoteAverage() * 5) / 10);
            ratingBarMovieVote.invalidate();
            textViewMovieTitle.setText(movie.getTitle());
        }
    }

}
