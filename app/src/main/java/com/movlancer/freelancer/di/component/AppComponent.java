package com.movlancer.freelancer.di.component;

import com.movlancer.freelancer.di.module.AppModule;
import com.movlancer.freelancer.view.MainActivity;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(MainActivity mainActivity);

}
