package com.movlancer.freelancer.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.movlancer.freelancer.MovLancerApp;
import com.movlancer.freelancer.R;
import com.movlancer.freelancer.adapter.MovieAdapter;
import com.movlancer.freelancer.model.Movie;
import com.movlancer.freelancer.model.MovieItem;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity implements MainActivityView {

    @Inject
    Retrofit retrofit;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rvMovie)
    RecyclerView rvMovie;
    @BindView(R.id.textViewMovieEmpty)
    TextView textViewMovieEmpty;
    @BindView(R.id.tvMovieHeader)
    TextView tvMovieHeader;
    @BindView(R.id.pbPagination)
    ProgressBar pbPagination;
    @BindView(R.id.pbRefresh)
    ProgressBar pbRefresh;

    private GridLayoutManager gridLayoutManager;
    private MovieAdapter movieAdapter;
    private MovieItem movieItem;

    private String queryMovie;
    private boolean userScrolled = true;
    private int pastVisiblesItems;
    private int visibleItemCount;
    private int totalItemCount;

    private List<Movie> movieList = new ArrayList<>();

    private MainActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ((MovLancerApp) getApplication()).getComponent().inject(this);
        presenter = new MainActivityPresenter(this, retrofit);
        setSupportActionBar(toolbar);
        setUpAdapter();
        loadPopularMovies(1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setQueryHint(getString(R.string.searching_movies));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                searchView.clearFocus();
                movieList.clear();
                queryMovie = s;
                searchMovies(queryMovie, 1);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (TextUtils.isEmpty(s)) {
                    queryMovie = s;
                    movieList.clear();
                    loadPopularMovies(1);
                }
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void setUpAdapter() {

        gridLayoutManager = new GridLayoutManager(this, 2);
        rvMovie.setLayoutManager(gridLayoutManager);
        movieAdapter = new MovieAdapter(this);
        rvMovie.setAdapter(movieAdapter);

        rvMovie.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;
                }
            }
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = gridLayoutManager.getChildCount();
                totalItemCount = gridLayoutManager.getItemCount();
                pastVisiblesItems = gridLayoutManager.findFirstVisibleItemPosition();
                if (userScrolled && (visibleItemCount + pastVisiblesItems) == totalItemCount) {
                    userScrolled = false;
                    pbPagination.setVisibility(View.VISIBLE);
                    if (TextUtils.isEmpty(queryMovie)) {
                        loadPopularMovies((int) (movieItem.getPage() + 1));
                    } else {
                        searchMovies(queryMovie, (int) (movieItem.getPage() + 1));
                    }
                }
            }
        });
    }

    private void loadPopularMovies(int page) {
        presenter.getMoviesByPopularity(page);
    }

    private void searchMovies(String query, int page) {
        presenter.getMoviesByName(query, page);
    }

    @Override
    public void setMovies(List<Movie> movies) {
        this.movieList.addAll(movies);
        movieAdapter.updateMovieAdapter(this.movieList);

        pbPagination.setVisibility(View.GONE);
        rvMovie.setVisibility(movies.isEmpty() ? View.GONE : View.VISIBLE);
        textViewMovieEmpty.setText(movies.isEmpty() ? getString(R.string.no_movies_found) : getString(R.string.loading));
        textViewMovieEmpty.setVisibility(movies.isEmpty() ? View.VISIBLE : View.GONE);
        pbRefresh.setVisibility(movies.isEmpty() ? View.VISIBLE : View.GONE);
        tvMovieHeader.setText(TextUtils.isEmpty(queryMovie) ? getString(R.string.most_popular_movies) : getString(R.string.movies_found));
    }

    @Override
    public void setMoviePage(MovieItem movieItem) {
        this.movieItem = movieItem;
        pbPagination.setVisibility(View.GONE);
    }

    @Override
    public void onError(String message) {
        tvMovieHeader.setVisibility(View.GONE);
        textViewMovieEmpty.setVisibility(View.VISIBLE);
        textViewMovieEmpty.setText(R.string.error_occured);
    }
}
