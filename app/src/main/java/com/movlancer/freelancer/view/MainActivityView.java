package com.movlancer.freelancer.view;

import com.movlancer.freelancer.model.Movie;
import com.movlancer.freelancer.model.MovieItem;

import java.util.List;

public interface MainActivityView {

    void setMovies(List<Movie> movies);

    void setMoviePage(MovieItem movieItem);

    void onError(String message);
}
