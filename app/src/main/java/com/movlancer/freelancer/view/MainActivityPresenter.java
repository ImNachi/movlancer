package com.movlancer.freelancer.view;

import com.movlancer.freelancer.model.MovieItem;
import com.movlancer.freelancer.network.ApiService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivityPresenter {

    private MainActivityView mainActivityView;
    private Retrofit retrofit;

    public MainActivityPresenter(MainActivityView mainActivityView, Retrofit retrofit) {
        this.mainActivityView = mainActivityView;
        this.retrofit = retrofit;
    }

    void getMoviesByPopularity(long page) {
        ApiService apiService = retrofit.create(ApiService.class);
        Call<MovieItem> call = apiService.getMoviesByPopularity(String.valueOf(page), "popularity.desc" ,"c6cd328edc5503383a2b600f3a5bd494");
        call.enqueue(new Callback<MovieItem>() {
            @Override
            public void onResponse(Call<MovieItem> call, Response<MovieItem> response) {

                mainActivityView.setMovies(response.body().getResults());
                mainActivityView.setMoviePage(response.body());
            }

            @Override
            public void onFailure(Call<MovieItem> call, Throwable t) {
                mainActivityView.onError(t.getMessage());
            }
        });
    }

    void getMoviesByName(String name, long page) {
        ApiService apiService = retrofit.create(ApiService.class);
        Call<MovieItem> call = apiService.getMoviesByName(name,  String.valueOf(page),"popularity.desc" , "c6cd328edc5503383a2b600f3a5bd494");
        call.enqueue(new Callback<MovieItem>() {
            @Override
            public void onResponse(Call<MovieItem> call, Response<MovieItem> response) {

                mainActivityView.setMovies(response.body().getResults());
                mainActivityView.setMoviePage(response.body());
            }

            @Override
            public void onFailure(Call<MovieItem> call, Throwable t) {
                mainActivityView.onError(t.getMessage());
            }
        });
    }
}
