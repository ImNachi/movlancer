package com.movlancer.freelancer;

import android.app.Application;

import com.movlancer.freelancer.di.component.AppComponent;
import com.movlancer.freelancer.di.component.DaggerAppComponent;
import com.movlancer.freelancer.di.module.AppModule;

public class MovLancerApp extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize Dagger Component
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this, getString(R.string.BASE_URL)))
                .build();
    }

    public AppComponent getComponent() {
        return appComponent;
    }
}
